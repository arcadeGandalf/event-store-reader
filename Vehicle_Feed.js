options({
  resultStreamName: 'Vehicle_Feed',
  $includeLinks: true,
  reorderEvents: false,
  processingLag: 0
});

fromCategory('Vehicle')
  .foreachStream()
  .when({
      Ordered: function (state, event) {
          linkTo('Vehicle_Feed', event);
      }
  });