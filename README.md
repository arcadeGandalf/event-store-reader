# event-store-reader

## Requirements
- Event Store
  - Use the following command to spin up EventStore in a Docker container
    - `docker run --name eventstore-node -it -p 2113:2113 -p 1113:1113 -e EVENTSTORE_RUN_PROJECTIONS=All -e EVENTSTORE_START_STANDARD_PROJECTIONS=true eventsto
re/eventstore`

## Setup
- Event Store projection
  - Create the projection in event store using the `Vehicle_Feed.js` file
  - Ensure the "Emit" option is ticked