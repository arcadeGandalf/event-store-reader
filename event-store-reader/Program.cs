﻿using System;

namespace event_store_reader
{
    using System.Net;
    using System.Text;
    using EventStore.ClientAPI;
    using EventStore.ClientAPI.SystemData;
    using Newtonsoft.Json;

    class Program
    {
        const string STREAM = "Vehicle_Feed";
        const string GROUP = "one-vehicle";
        const int DEFAULTPORT = 1113;

        static void Main(string[] args)
        {

            var settings = ConnectionSettings.Create();
            using (var conn = EventStoreConnection.Create(settings, new IPEndPoint(IPAddress.Loopback, DEFAULTPORT)))
            {
                conn.ConnectAsync().Wait();

                CreateSubscription(conn);

                conn.ConnectToPersistentSubscription(STREAM, GROUP, (_, x) =>
                {
                    var eventDataString = Encoding.UTF8.GetString(x.Event.Data);
                    if (x.Event.EventType == "Ordered")
                    {
                        var vehicleOrdered = JsonConvert.DeserializeObject<VehicleOrderedEvent>(eventDataString);
                        Console.WriteLine(JsonConvert.SerializeObject(vehicleOrdered));
                    }
                });

                Console.WriteLine("waiting for events. press enter to exit");
                Console.ReadLine();
            }

        }

        private static void CreateSubscription(IEventStoreConnection conn)
        {
            PersistentSubscriptionSettings settings = PersistentSubscriptionSettings.Create()
                .ResolveLinkTos()
                .StartFromCurrent();

            try
            {
                conn.CreatePersistentSubscriptionAsync(STREAM, GROUP, settings, new UserCredentials("admin", "changeit")).Wait();
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException.GetType() != typeof(InvalidOperationException)
                    && ex.InnerException?.Message != $"Subscription group {GROUP} on stream {STREAM} already exists")
                {
                    throw;
                }
            }
        }
    }
}
